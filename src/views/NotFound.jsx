import React, { Fragment } from 'react';
import Header from '../components/Header';
import { withStyles } from "@material-ui/core/styles";
import logo from '../img/logo.svg';

const styles = {
  notFound: {
    width: '70%',
    heigth: '50%',
    margin: '0% 15%',
    textAlign: 'center',
    fontSize: '35px',
    '& > img': {
      width: '60%',
    },
    '& > h1': {
      marginTop:'0px',
    }
  },
};

const NotFound = props => {
  const { classes } = props;
  return (
    <Fragment>
      <Header/>
      <div className={classes.notFound}>
      <img
        alt='Icon'
        src={logo} />
        <h1> 404 Not Found</h1>
      </div>
    </Fragment>
  );
};

export default  withStyles(styles)(NotFound);
