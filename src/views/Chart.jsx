import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../store/actions";
import { ToastContainer } from "react-toastify";
import Header from "../components/Header";
import Wrapper from "../components/Wrapper";
import LiveChart from "../components/LiveChart";

const mapStateToProps = state => ({
  arrayDroneData: state.drone.arrayDroneData,
  droneData: state.drone.droneData,
  arrayAllDroneInformation: state.drone.arrayAllDroneInformation,
  loading: state.drone.loading,
});

class Chart extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: actions.FETCH_DRONE,
    })
  }

  render() {
    const {
      arrayAllDroneInformation,
      arrayDroneData,
      droneData,
      history,
      loading,
    } = this.props;

    return (
      <Wrapper>
        <Header/>
        <LiveChart
          arrayAllDroneInformation={arrayAllDroneInformation}
          arrayDroneData={arrayDroneData}
          droneData={droneData}
          history={history}
          loading={loading}
        />
      <ToastContainer />
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps)(Chart);