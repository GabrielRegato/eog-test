import * as actions from "../actions";
import { THIRTY_MIN } from '../../constants/constants';

const initialState = {
  initialTimestamp: 0,
  arrayAllDroneInformation: [],
  loading: false,
  arrayDroneData: [],
  droneData: {},
};

const startLoading = (state, action) => {
  return { ...state, loading: true };
};

const droneDataReceived = (state, action) => {
  const { data } = action;
  const { arrayDroneData, arrayAllDroneInformation, initialTimestamp } = state;
  const lastItem = data ? data.pop() : {};
  const arrayDataInformation = arrayDroneData;
  arrayDataInformation.push(lastItem);

  let arrayAllInformation = arrayAllDroneInformation;
  let startTimestamp = initialTimestamp;

  if (arrayAllInformation.length === 0 ) {
    startTimestamp = Math.round((new Date()).getTime() / 1000);
    arrayAllInformation = data;
  } else {
    const nowTimeStamp = Math.round((new Date()).getTime() / 1000);
    if (( nowTimeStamp - initialTimestamp ) < THIRTY_MIN) {
      arrayAllInformation.push(lastItem);
    }
  }

  return {
    ...state,
    loading: false,
    arrayDroneData: arrayDataInformation,
    droneData: lastItem,
    arrayAllDroneInformation: arrayAllInformation,
    initialTimestamp: startTimestamp,
  };
};

const handlers = {
  [actions.FETCH_DRONE]: startLoading,
  [actions.DRONE_DATA_RECEIVED]: droneDataReceived,
};

export default (state = initialState, action) => {
  const handler = handlers[action.type];
  if (typeof handler === "undefined") return state;
  return handler(state, action);
};
