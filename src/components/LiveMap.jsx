import React from 'react';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';

// Import constants file
import {API_GM_KEY} from '../constants/constants';

// Import Main Container
import Container from './Container';

const styles = {
    position:'relative',
    height: '500px',
};

const LiveMap = props => {
  const {
    history,
    loading,
  } = props;

  return (
    <Container
      title={'Live Map'}
      nodeTag={map(props)}
      loading={loading}
      history={history}
      extraClass={styles}
    />
  );
};

const map = props => {
  const { google, droneData } = props;

  const mapStyle = {
    width: '95%',
    height: '75%'
  };

  const dronePosition = {
    lat: droneData.latitude,
    lng: droneData.longitude,
  };

  return (
    <Map
      google={google}
      zoom={4}
      style={mapStyle}
      initialCenter={dronePosition} >
      <Marker
        title={'Drone Actual Position'}
        position={dronePosition} />
    </Map>);
};

export default GoogleApiWrapper({apiKey: API_GM_KEY})(LiveMap);