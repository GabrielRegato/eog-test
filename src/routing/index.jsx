import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import App from '../App';
import Chart from '../views/Chart';
import Dashboard from '../views/Dashboard';
import Map from '../views/Map';
import NotFound from '../views/NotFound';

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact={true} path={'/'} component={App} />
        <Route path={ '/dashboard' } component={Dashboard} />
        <Route path={ '/live-map' } component={Map} />
        <Route path={ '/chart' } component={Chart} />
        <Route component={NotFound} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;