import React from 'react';
import Card from '@material-ui/core/Card';
import CardHeaderRaw from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import Button from '@material-ui/core/Button';

const cardStyles = theme => ({
  root: {
    background: theme.palette.primary.main
  },
  title: {
    color: 'white',
    fontSize: '20px',
  }
});
const CardHeader = withStyles(cardStyles)(CardHeaderRaw);

const styles = {
  card: {
    margin: '5% 5%',
  },
  button: {
    margin: '10px',
    backgroundColor: '#f44336',
    color: '#ffffff',
    '&:hover': {
      backgroundColor:'#d50000',
    }
  },
};

const Container = props => {
  const {
    title,
    nodeTag,
    classes,
    history,
    loading,
    extraClass,
  } = props;

  if (loading) return <LinearProgress />;
  return (
    <Card className={classes.card} style={extraClass}>
      <CardHeader
        title={title}
        avatar={
          <Button
              className={classes.button}
              onClick={ () => history.push('/') } >
            Return
          </Button>
        }
      />
      <CardContent>
        {nodeTag}
      </CardContent>
    </Card>
  );
};

export default withStyles(styles)(Container);
