import React from "react";
import "react-toastify/dist/ReactToastify.css";
import Header from "./components/Header";
import Wrapper from "./components/Wrapper";
import Menu from "./components/Menu";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  menuItem: {
    '&:focus': {
      backgroundColor: theme.palette.primary.main,
      '& $primary, & $icon': {
        color: theme.palette.common.white,
      },
    },
  },
  primary: {},
  icon: {},
});

const App = props => {
  const { history } = props;
  return (
    <Wrapper>
      <Header />
      <Menu history={history} />
    </Wrapper> );
}

export default withStyles(styles)(App);
