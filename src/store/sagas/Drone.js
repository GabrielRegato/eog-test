import { takeEvery, call, put, cancel, all, } from "redux-saga/effects";
import API from "../api";
import * as actions from "../actions";

function delay(duration) {
  const promise = new Promise(resolve => {
    setTimeout(() => resolve(true), duration)
  })
  return promise
}

function* watchFetchDrone(action) {
  while (true) {
    try {
      const { data, error } = yield call(
        API.findDrone,
      );
      if (error) {
        yield put({ type: actions.API_ERROR, code: error.code });
        yield cancel();
        return;
      }
      yield put({ type: actions.DRONE_DATA_RECEIVED, data });
      yield call(delay, 3000)
    } catch (e) {
      console.log('el e', e);
      yield put({ type: actions.API_ERROR, code: e.code });
      yield cancel();
      return;
    }
  }
}

function* watchAppLoad() {
  yield all([
    takeEvery(actions.FETCH_DRONE, watchFetchDrone),
  ]);
}

export default [watchAppLoad];
