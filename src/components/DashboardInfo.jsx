import React from 'react';

// Material UI modules
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';

// Import Utilities
import { convertDate, isEmpty } from '../utilities/helpers';

// Import Main Container
import Container from './Container';

const DashboardInfo = props => {
  const {
    droneData,
    loading,
    history,
  } = props;

  return !isEmpty(droneData) && (
    <Container
      title={'Dron Information'}
      nodeTag={dashboard(props)}
      loading={loading}
      history={history}
    />
  );
};

const dashboard = props => {
  const {
    droneData,
  } = props;

  let dateData = '';
  if ( !isEmpty(droneData) ) {
    const date = convertDate(droneData.timestamp);
    dateData = `${date.month} - ${date.day} / ${date.hours}:${date.minutes}:${date.seconds}`;
  }

  return (
    <List>
      <ListItem>
        <ListItemText primary='Temperature:' />
        <Typography component='p'>{droneData.metric}</Typography>
      </ListItem>
      <ListItem>
        <ListItemText primary='Latitude:' />
        <Typography component='p'>{droneData.latitude}</Typography>
      </ListItem>
      <ListItem>
        <ListItemText primary='Longitude:' />
        <Typography component='p'>{droneData.longitude}</Typography>
      </ListItem>
      <ListItem>
        <ListItemText primary='Last Received:' />
        <Typography component='p'>{dateData}</Typography>
      </ListItem>
    </List> );
};

export default DashboardInfo;
