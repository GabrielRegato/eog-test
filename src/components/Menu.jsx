import React from "react";
import Card from "@material-ui/core/Card";
import CardHeaderRaw from "@material-ui/core/CardHeader";
import CardContent from "@material-ui/core/CardContent";
import ListItemText from "@material-ui/core/ListItemText";
import { withStyles } from "@material-ui/core/styles";
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
// Menu icons
import { Dashboard, Satellite, InsertChart } from '@material-ui/icons';

const cardStyles = theme => ({
  root: {
    background: theme.palette.primary.main
  },
  title: {
    color: "white"
  }
});
const CardHeader = withStyles(cardStyles)(CardHeaderRaw);

const styles = {
  card: {
    margin: "5% 25%"
  }
};

const Menu = props => {
  const { classes, history } = props;
  return (
    <Card className={classes.card}>
      <CardHeader title="EOG Test Menu" />
      <CardContent>
        <MenuList>
          <MenuItem
            className={classes.menuItem}
            onClick={() => history.push('/dashboard')} >
            <ListItemIcon className={classes.icon}>
              <Dashboard />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              inset
              primary='Drone Dashboard' />
          </MenuItem>
          <MenuItem
            className={classes.menuItem}
            onClick={() => history.push('/live-map')} >
            <ListItemIcon className={classes.icon}>
              <Satellite />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              inset
              primary='Live Map' />
          </MenuItem>
          <MenuItem
            className={classes.menuItem}
            onClick={() => history.push('/chart')} >
            <ListItemIcon className={classes.icon}>
              <InsertChart />
            </ListItemIcon>
            <ListItemText
              classes={{ primary: classes.primary }}
              inset
              primary='Live Chart' />
          </MenuItem>
        </MenuList>
      </CardContent>
    </Card>
  );
};

export default withStyles(styles)(Menu);
