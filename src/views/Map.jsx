import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../store/actions";
import { ToastContainer } from "react-toastify";
import Header from "../components/Header";
import Wrapper from "../components/Wrapper";
import LiveMap from "../components/LiveMap";

const mapStateToProps = state => ({
  arrayDroneData: state.drone.arrayDroneData,
  droneData: state.drone.droneData,
  loading: state.drone.loading,
});

class Map extends Component {
  componentDidMount() {
    const { dispatch } = this.props;
    dispatch({
      type: actions.FETCH_DRONE,
    })
  }

  render() {
    const {
      arrayDroneData,
      droneData,
      history,
      loading,
    } = this.props;

    return (
      <Wrapper>
        <Header/>
        <LiveMap
          arrayDroneData={arrayDroneData}
          droneData={droneData}
          history={history}
          loading={loading} />
      <ToastContainer />
      </Wrapper>
    );
  }
}

export default connect(mapStateToProps)(Map);