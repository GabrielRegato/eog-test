export const convertDate = timestamp => {
  const dateObj = {};
  // Months array
  const arrayMonths = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

  const date = new Date(timestamp * 1000);

  dateObj.month = arrayMonths[date.getMonth()];
  dateObj.day = date.getDate();
  dateObj.hours = date.getHours();

  const minutes = "0" + date.getMinutes();
  const seconds = "0" + date.getSeconds();

  dateObj.minutes = minutes.substr(-2);
  dateObj.seconds = seconds.substr(-2);

  return dateObj;
};

export const isEmpty = object => {
  for ( const key in object ) {
    if ( object.hasOwnProperty(key) )
      return false;
  }
  return true;
};