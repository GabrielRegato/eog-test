import React from 'react';
import {Chart} from 'react-google-charts';

// Import Utilities
import { convertDate, isEmpty } from '../utilities/helpers';

// Import Main Container
import Container from './Container';

const LiveChart = props => {
  const {
    history,
    loading,
  } = props;

  return (
    <Container
      title={'Live Drone Chart'}
      nodeTag={chart(props)}
      loading={loading}
      history={history}
    />
  );
};

const chart = props => {
  const {
    arrayAllDroneInformation,
    droneData,
  } = props;

  let dateData = '';
  if ( !isEmpty(droneData) ) {
    const date = convertDate(droneData.timestamp);
    dateData = `${date.month} - ${date.day}`;
  }

  return (
    <Chart
      height={'400px'}
      chartType='LineChart'
      loader={<div>Loading Chart</div>}
      data={dataChartInformation(arrayAllDroneInformation)}
      options={{
        width:'100%',
        hAxis: {
          title: `Date: ${dateData}`,
        },
        vAxis: {
          title: 'Metric',
        },
      }}
      rootProps={{ 'data-testid': '1' }}
    />);
};

const dataChartInformation = data => {
  const dataChart = [['x', 'Drone Temperature']];

  data.map((chartInfo) => {
    const chart = [];
    const convertTime = convertDate(chartInfo.timestamp);
    const timeLabel = `${convertTime.hours}:${convertTime.minutes}`;
    chart.push(timeLabel);
    chart.push(chartInfo.metric);
    return dataChart.push(chart);
  });

  return dataChart;
};

export default LiveChart;
